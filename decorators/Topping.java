package decorators;

import objects.Component;

public class Topping extends Decorator {
    public Topping(Component component) {
        super(component);
    }

    @Override
    void afterCooking() {
        System.out.println("Добавлен сироп");
    }
}
