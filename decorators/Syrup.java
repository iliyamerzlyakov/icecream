package decorators;

import objects.Component;

public class Syrup extends Decorator {
    public Syrup(Component component) {
        super(component);
    }
    @Override
    void afterCooking() {
        System.out.println("Добавлен сироп");
    }
}
